const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")
const { response } = require("express")

dotenv.config()

const app = express()
const port = 3001

mongoose.connect(`mongodb+srv://ivanc:${process.env.MONGODB_PASSWORD}@cluster0.asallxv.mongodb.net/?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', () => console.error("Connection error."))
db.on('open', () => console.log("Connected to MongoDB!"))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// 1. Create a User schema.
const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

// 2. Create a User model.
const User = mongoose.model('User', userSchema)

// 3. Create a POST route that will access the /signup route that will create a user.
app.post('/signup', (request, response) => {
    User.findOne({ username: request.body.username }, (error, result) => {
        if (result != null && result.username == request.body.username) {
            return response.send('Duplicate user found!')
        }
        let newUser = new User({
            username: request.body.username
        })
        newUser.save((error, savedTask) => {
            if (error) {
                return console.error(error)
            }
            else {
                return response.status(200).send('New user created!')
            }
        })
    })
})

app.listen(port, () => console.log(`Server running at localhost:${port}`))

